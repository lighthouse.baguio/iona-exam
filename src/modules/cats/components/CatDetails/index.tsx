import { Breed } from "../../types";
import { FC } from "react";

type Props = {
  breed: Breed;
};

const CatDetails: FC<Props> = ({ breed }) => {
  return (
    <>
      <h3>{breed.name}</h3>
      <h4>Origin: {breed.origin}</h4>
      <h6>{breed.temperament}</h6>
      <p>{breed.description}</p>
    </>
  );
};

export default CatDetails;
