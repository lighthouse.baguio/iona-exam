import { FormSelect, Placeholder } from "react-bootstrap";
import { FC, useEffect, useState } from "react";
import { Breed } from "../../types";
import getBreeds from "../../endpoints/getBreeds";

type Props = {
  onBreedChange: (breed: Breed) => any;
};

const BreedSelector: FC<Props> = ({ onBreedChange }) => {
  const [breeds, setBreeds] = useState<Breed[]>([]);
  const [loading, setLoading] = useState(false);

  const isLoading = !breeds || loading;

  useEffect(() => {
    const loadData = async () => {
      setLoading(true);
      const { data } = await getBreeds();
      setBreeds(data);
      setLoading(false);
    };
    loadData();
  }, []);

  return (
    <div>
      <label>Breed {isLoading && "(Loading..)"}</label>
      {isLoading ? (
        <Placeholder animation={"wave"} as={"p"}>
          <Placeholder size={"lg"} xs={12} />
        </Placeholder>
      ) : (
        <FormSelect
          onChange={(e) => {
            const breed = breeds.find(
              (_breed) => _breed.id === e.currentTarget.value
            ) as unknown as Breed;
            onBreedChange(breed);
          }}
        >
          <option value={""}>Select Breed</option>
          {breeds?.map((breed) => {
            return (
              <option key={`breed-${breed.id}`} value={breed.id}>
                {breed.name}
              </option>
            );
          })}
        </FormSelect>
      )}
    </div>
  );
};

export default BreedSelector;
