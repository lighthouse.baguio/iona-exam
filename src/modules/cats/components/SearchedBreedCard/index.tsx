import { SearchedImage } from "../../types";
import { FC } from "react";
import { Button, Card } from "react-bootstrap";
import CatDetails from "../CatDetails";

type Props = {
  searchedBreed: SearchedImage;
  onBreedSelected?: (breed: SearchedImage) => any;
  preview?: boolean;
};

const SearchedBreedCard: FC<Props> = ({
  searchedBreed,
  onBreedSelected,
  preview = true,
}) => {
  return (
    <Card>
      <Card.Img variant="top" src={searchedBreed.url} />
      <Card.Body>
        <div className={"d-grid"}>
          {preview ? (
            <Button
              variant={"primary"}
              onClick={() => onBreedSelected && onBreedSelected(searchedBreed)}
            >
              View Details
            </Button>
          ) : (
            <CatDetails breed={searchedBreed.breeds[0]} />
          )}
        </div>
      </Card.Body>
    </Card>
  );
};

export default SearchedBreedCard;
