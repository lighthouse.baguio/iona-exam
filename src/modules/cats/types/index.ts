export interface Breed {
  id: string;
  name: string;
  alt_names: string;
  origin: string;
  description: string;
  temperament: string;
}

export interface SearchedImage {
  height: number;
  id: string;
  url: string;
  width: number;
  breeds: Breed[];
}
