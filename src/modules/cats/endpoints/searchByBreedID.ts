import { Breed } from "../types";
import service from "../../../services/axios/service";

const searchByBreedID = (breed: Breed, page = 1) => {
  return service().get("images/search", {
    params: {
      page,
      limit: 10,
      breed_id: breed.id,
    },
  });
};

export default searchByBreedID;
