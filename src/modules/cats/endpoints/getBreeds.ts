import service from "../../../services/axios/service";

const getBreeds = () => {
  return service().get("/breeds");
};

export default getBreeds;
