import service from "../../../services/axios/service";

const getBreedDetailsByImageID = (searchedImageID: string) => {
  return service().get(`images/${searchedImageID}`);
};

export default getBreedDetailsByImageID;
