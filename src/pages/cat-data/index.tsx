import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { SearchedImage } from "../../modules/cats/types";
import getBreedDetailsByImageID from "../../modules/cats/endpoints/getBreedDetailsByImageID";
import SearchedBreedCard from "../../modules/cats/components/SearchedBreedCard";

const CatDataPage = () => {
  const { catID } = useParams();
  const [searchedImage, setSearchedImage] = useState<SearchedImage | undefined>(
    undefined
  );
  useEffect(() => {
    const loadData = async () => {
      if (!catID) {
        return;
      }
      const { data } = await getBreedDetailsByImageID(catID);
      setSearchedImage(data);
    };
    loadData();
  }, [catID]);

  return (
    <>
      <h3>Cat Details</h3>
      {searchedImage && (
        <SearchedBreedCard searchedBreed={searchedImage} preview={false} />
      )}
    </>
  );
};

export default CatDataPage;
