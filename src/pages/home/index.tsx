import BreedSelector from "../../modules/cats/components/BreedSelector";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { Breed, SearchedImage } from "../../modules/cats/types";
import searchByBreedID from "../../modules/cats/endpoints/searchByBreedID";
import SearchedBreedCard from "../../modules/cats/components/SearchedBreedCard";
import { Col, Container, Row } from "react-bootstrap";

const HomePage = () => {
  const navigate = useNavigate();
  const [searchedBreeds, setSearchBreeds] = useState<SearchedImage[]>([]);
  const [selectedBreed, setSelectedBreed] = useState<Breed | undefined>(
    undefined
  );
  const [currentPage, setCurrentPage] = useState<number>(1);

  const handleBreedSelected = (selectedImage: SearchedImage) => {
    navigate(`/cats/${selectedImage.id}`);
  };

  useEffect(() => {
    const loadData = async () => {
      if (!selectedBreed) {
        return;
      }
      const { data } = await searchByBreedID(selectedBreed, currentPage);

      if (currentPage === 1) {
        setSearchBreeds(data);
      } else {
        setSearchBreeds([...searchedBreeds, ...data]);
      }
    };
    loadData();
  }, [selectedBreed, currentPage]);

  return (
    <>
      <h3>Cat Browser</h3>
      <BreedSelector
        onBreedChange={(breed) => {
          setCurrentPage(1);
          setSelectedBreed(breed);
        }}
      />
      {searchedBreeds.length === 0 && <em>No cats found</em>}
      <div style={{ paddingTop: "24px" }}>
        <Container>
          <Row>
            {searchedBreeds.map((searchedBreed) => {
              return (
                <Col sm={4} xs={12} key={`breed-search-${searchedBreed.id}`}>
                  <SearchedBreedCard
                    searchedBreed={searchedBreed}
                    onBreedSelected={handleBreedSelected}
                  />
                </Col>
              );
            })}
          </Row>
        </Container>
      </div>
    </>
  );
};

export default HomePage;
