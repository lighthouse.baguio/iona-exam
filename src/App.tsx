import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./pages/home";
import CatDataPage from "./pages/cat-data";
import { Container } from "react-bootstrap";

function App() {
  return (
    <Container>
      <BrowserRouter>
        <Routes>
          <Route path={"cats/:catID"} element={<CatDataPage />} />
          <Route path={"/"} element={<HomePage />} />
        </Routes>
      </BrowserRouter>
    </Container>
  );
}

export default App;
