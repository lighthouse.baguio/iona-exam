import axios from "axios";

const service = () => {
  return axios.create({
    baseURL: "https://api.thecatapi.com/v1/",
    headers: {
      "x-api-key": "f49591d2-11aa-497b-b19b-add92ad78902",
    },
  });
};

export default service;
